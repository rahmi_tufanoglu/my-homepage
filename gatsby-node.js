const { createFilePath } = require(`gatsby-source-filesystem`);
const path = require(`path`);

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions;

  if (node.internal.type === "MarkdownRemark") {
    const slug = createFilePath({
      node,
      getNode,
      basePath: "markdown-pages",
    });

    createNodeField({
      node,
      value: slug,
      name: "slug",
    });
  }
};

exports.createFilePath = async ({ graphql, actions }) => {
  const { createPage } = actions;

  const result = await graphql(`
    query {
      allMarkdownRemark {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
    }
  `);

  result.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      context: {
        path: `/posts/${node.fields.slug}`,
        component: path.resolve("./src/pages/my-page.tsx"),
        context: {
          slug: node.fields.slug,
        },
      },
    });
  });
};

/*exports.createPages = async ({ actions: { createPage }, graphql }) => {
  const results = await graphql(`
    query {
      allMarkdownRemark {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
    }
  `);

  if (results.error) {
    console.log(results.error);
  }

  result.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      context: {
        path: `/posts/${node.fields.slug}`,
        component: path.resolve("./src/pages/my-page.tsx"),
        context: {
          slug: node.fields.slug,
        },
      },
    });
  });
};*/

exports.onCreatePage = async ({ page, actions }) => {
  const { deletePage } = actions;

  if (page.path === "/my-page/") {
    deletePage(page);
  }
};
