import { useState, SetStateAction, Dispatch } from "react";

export function useFormFields(initialState: any) {
  const [fields, setFields] = useState(initialState);

  return [
    fields,
    function(event: React.ChangeEvent<HTMLInputElement>) {
      setFields({
        ...fields,
        [event.target.id]: event.target.value,
      });
    },
  ];
}
