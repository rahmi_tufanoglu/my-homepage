import React from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { Typography, Grid, Box } from "@material-ui/core";
import ServiceBox from "../components/service-box";
import { graphql } from "gatsby";
import { FluidObject } from "gatsby-image";

const useStyles = makeStyles((theme: Theme) => ({
  textContainer: {
    display: "flex",
    flexDirection: "column",
    textAlign: "center",
    marginLeft: "10%",
    marginRight: "10%",
  },
}));

export const query = graphql`
  query {
    avatar: file(relativePath: { eq: "gatsby-icon.png" }) {
      childImageSharp {
        fluid(maxWidth: 500) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`;

interface Data {
  avatar: {
    childImageSharp: {
      fluid: FluidObject;
    };
  };
}

interface Props {
  data: Data;
}

const Service = ({ data }: Props) => {
  const classes = useStyles();
  const { avatar } = data;

  // TODO: Text in RICHTEXT
  return (
    <>
      <div className={classes.textContainer}>
        <Box mb={6}>
          <Typography variant="h4">
            <Box fontWeight="fontWeightBold">UNSERE LESITUNGEN FÜR EUCH</Box>
          </Typography>
        </Box>
        <Box mb={6}>
          <Typography variant="h5">
            ZEIT BEDEUTET GELD, DAS HABEN WIR VERSTANDEN.
          </Typography>
        </Box>
        <Typography variant="h5">
          DESHALB ENTWICKELN WIR FÜR UNSERE KUNDEN NICHT NUR INNOVATIVE
          WEBANWENDUNGEN. WIR ENTWICKELN SIE SO, DASS IHR EUER MARKTPOTENZIAL
          OPTIMAL REALISIEREN KÖNNT. GEMEINSAM ERREICHEN WIR EURE ZIELE.
        </Typography>
      </div>
      <Box m={6}>
        <Grid container spacing={6}>
          <Grid item sm={4} xs={12}>
            <ServiceBox avatar={avatar.childImageSharp.fluid} />
          </Grid>
          <Grid item sm={4} xs={12}>
            <ServiceBox avatar={avatar.childImageSharp.fluid} />
          </Grid>
          <Grid item sm={4} xs={12}>
            <ServiceBox avatar={avatar.childImageSharp.fluid} />
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default Service;
