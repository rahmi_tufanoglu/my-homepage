import React from "react";
import { graphql } from "gatsby";

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      frontmatter {
        author
        title
        date
      }
    }
  }
`;

interface Data {
  markdownRemark: {
    frontmatter: {
      author: string;
      title: string;
      date: string;
    };
  };
}

interface MyPageProps {
  data: Data;
}

const MyPage = ({ data }: MyPageProps) => {
  const { markdownRemark } = data;

  return (
    <div>
      <h2>{markdownRemark.frontmatter.title}</h2>
      <h2>{markdownRemark.frontmatter.date}</h2>
      <h2>{markdownRemark.frontmatter.author}</h2>
    </div>
  );
};

export default MyPage;
