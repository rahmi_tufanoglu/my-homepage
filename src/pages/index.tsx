import React from "react";
import Layout from "../components/layout";
import {
  TopImage,
  SocialMediaBar,
  Footer,
  HeroSection,
  BannerSection,
  About,
  Bottom,
} from "../components";
import { graphql, navigate } from "gatsby";
import { FluidObject } from "gatsby-image";

export const query = graphql`
  query {
    site {
      siteMetadata {
        title
        description
        author
      }
    }
    bg1: file(relativePath: { eq: "background-1.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 2000) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    bg2: file(relativePath: { eq: "background-2.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 2000) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    bg3: file(relativePath: { eq: "background-3.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 2000) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    bg4: file(relativePath: { eq: "background-4.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 2000) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`;

interface Data {
  site: {
    siteMetadata: {
      title: string;
      description: string;
      author: string;
    };
  };
  bg1: {
    childImageSharp: {
      fluid: FluidObject;
    };
  };
  bg2: {
    childImageSharp: {
      fluid: FluidObject;
    };
  };
  bg3: {
    childImageSharp: {
      fluid: FluidObject;
    };
  };
  bg4: {
    childImageSharp: {
      fluid: FluidObject;
    };
  };
}

interface IndexPageProps {
  data: Data;
}

const IndexPage = ({ data }: IndexPageProps) => {
  const {
    site: { siteMetadata },
    bg1,
    bg2,
    bg3,
    bg4,
  } = data;

  return (
    <Layout>
      <TopImage fluid={bg1.childImageSharp.fluid} />
      <HeroSection
        image1={bg2.childImageSharp.fluid}
        image2={bg3.childImageSharp.fluid}
        image3={bg4.childImageSharp.fluid}
      />
      <BannerSection />
      <About />
      <SocialMediaBar
        xingUrl=""
        twitterUrl=""
        facebookUrl=""
        instagramUrl=""
        youtubeUrl=""
      />
      <Bottom />
      <Footer />
      <button onClick={() => navigate("/page-3")}>PAGE 3</button>
      <button onClick={() => navigate("/page-4")}>PAGE 4</button>
      <button onClick={() => navigate("/page-5")}>Formular</button>
    </Layout>
  );
};

export default IndexPage;
