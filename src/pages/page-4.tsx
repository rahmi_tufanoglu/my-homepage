import React from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { Typography, Grid, Box } from "@material-ui/core";
import { graphql } from "gatsby";
import { FluidObject } from "gatsby-image";
import MemberBox from "../components/member-box";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
  },
  element: {},
}));

export const query = graphql`
  query {
    avatar: file(relativePath: { eq: "gatsby-icon.png" }) {
      childImageSharp {
        fluid(maxWidth: 500) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`;

interface Data {
  text?: string;
  avatar: {
    childImageSharp: {
      fluid: FluidObject;
    };
  };
}

interface Props {
  data: Data;
}

const test =
  "sadenwkjfnejn ebewg b grgi rhgbergnengkr egnipergpiernsdpkjnw wue wiergpiernsdpkjnw wue wiefnwrjng rwijgpi nwgwgprgkgpjaopjrgoae or gaeg pearkmgeakjgnepg egperng fnwrjng rwijgpi nwgwgprgkgpjaopjrgoae or gaeg pearkmgeakjgnepg egperng rgpiernsdpkjnw wue wiefnwrjng rwijgpi nwgwgprgkgpjaopjrgoae or gaeg pearkmgeakjgnepg egperng rgpiernsdpkjnw wue wiefnwrjng rwijgpi nwgwgprgkgpjaopjrgoae or gaeg pearkmgeakjgnepg egperng rgpiernsdpkjnw wue wiefnwrjng rwijgpi nwgwgprgkgpjaopjrgoae or gaeg pearkmgeakjgnepg egperng ";

const Team = ({ data }: Props) => {
  const classes = useStyles();
  const { avatar } = data;

  const avatarList = [
    {
      avatar: avatar.childImageSharp.fluid,
      name: "Vorname Nachname",
      text: test,
    },
    {
      avatar: avatar.childImageSharp.fluid,
      name: "Vorname Nachname",
      text: test,
    },
    {
      avatar: avatar.childImageSharp.fluid,
      name: "Vorname Nachname",
      text: test,
    },
    {
      avatar: avatar.childImageSharp.fluid,
      name: "Vorname Nachname",
      text: test,
    },
    {
      avatar: avatar.childImageSharp.fluid,
      name: "Vorname Nachname",
      text: test,
    },
    {
      avatar: avatar.childImageSharp.fluid,
      name: "Vorname Nachname",
      text: test,
    },
  ];

  return (
    <>
      <Box mb={10}>
        <Typography variant="h4">UNSER TEAM</Typography>
      </Box>
      <Grid container spacing={3}>
        <Grid item sm={6} xs={12}>
          <Typography variant="body1">{test}</Typography>
        </Grid>
        {avatarList.map((item, index) => {
          return (
            <Grid item sm={6} xs={12} key={index}>
              <MemberBox profilePicture={item.avatar} />
            </Grid>
          );
        })}
      </Grid>
    </>
  );
};

export default Team;
