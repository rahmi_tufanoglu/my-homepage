import React from "react";
import { graphql } from "gatsby";
import Layout from "../components/layout";
import CardContentBlog from "../components/card-content-blog";

export const query = graphql`
  query {
    allMarkdownRemark {
      edges {
        node {
          excerpt
          frontmatter {
            date
            title
            author
          }
          fields {
            slug
          }
        }
      }
    }
  }
`;

interface Test {
  background?: string;
  color?: string;
}

interface Data {
  allMarkdownRemark: {
    edges: [
      {
        node: {
          excerpt: string;
          frontmatter: {
            title: string;
            author: string;
            date: string;
          };
          fields: {
            slug: string;
          };
        };
      }
    ];
  };
}

interface Props {
  data: Data;
  test: Test;
}

const Page2 = ({ data }: Props) => {
  const {
    allMarkdownRemark: { edges: node },
  } = data;

  return (
    <Layout appBarPosition="sticky" title="Page 2">
      {node.map((item, index) => {
        return (
          <div key={index}>
            <CardContentBlog
              key={index}
              title={item.node.frontmatter.title}
              author={item.node.frontmatter.author}
              date={item.node.frontmatter.date}
              excerpt={item.node.excerpt}
              // slug={item.node.fields.slug}
            />
          </div>
        );
      })}
    </Layout>
  );
};

export default Page2;
