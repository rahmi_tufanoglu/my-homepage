import React, {
  useState,
  FormEvent,
  ChangeEvent,
  useCallback,
  useEffect,
} from "react";
import { Theme, makeStyles } from "@material-ui/core/styles";
import {
  Button,
  Box,
  Checkbox,
  FormControlLabel,
  Link,
  TextField,
  CircularProgress,
  Typography,
  Snackbar,
  Slide,
  TextareaAutosize,
} from "@material-ui/core";
import { Alert, AlertTitle } from "@material-ui/lab";
import { Link as GatsbyLink } from "gatsby";
import { TransitionProps } from "react-transition-group/Transition";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  button: {
    width: "50%",
    textTransform: "uppercase",
  },
  label: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  buttonProgress: {
    position: "absolute",
  },
  redText: {
    color: "red",
  },
  textArea: {
    width: "100%",
  },
}));

interface PostProps {}

interface Props {
  url: string;
}

const Form = ({ url }: Props) => {
  const classes = useStyles();
  /* const {
    isContactEnabled,
    isEmailValid,
    isButtonPressed,
    values,
    errors,
    helperTexts,
  } = useForm(); */
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [isContactEnabled, setIsContactEnabled] = useState(false);
  const [helperText, setHelperText] = useState();
  const [isButtonPressed, setIsButtonPressed] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(false);
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(false);
  const [open, setOpen] = React.useState(false);
  const timer = React.useRef<number>();

  const [post, setPost] = useState<PostProps>();
  //const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const fetchData = useCallback(async () => {
    setLoading(true);
    const response = await fetch(url);
    try {
      const data = await response.json();
      if (response.status === 200) {
        setPost(data);
      }
    } catch (error) {
      const e =
        response.status === 404
          ? "Resource not found"
          : "An unexpected error has occured";
      console.log(e);
    } finally {
      setLoading(false);
    }
  }, [url]);

  useEffect(() => {
    fetchData();
  }, [fetchData, url]);

  const onChangeName = (event: ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  };

  const emailCheck = (e: string) => {
    const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    reg.test(e.toLowerCase()) ? setIsEmailValid(true) : setIsEmailValid(false);
  };

  const onChangeEmail = (event: ChangeEvent<HTMLInputElement>) => {
    emailCheck(event.target.value);
    setEmail(event.target.value);
  };

  const onChangeMessage = (event: ChangeEvent<HTMLInputElement>) => {
    setMessage(event.target.value);
  };

  const onChangeContact = (event: ChangeEvent<HTMLInputElement>) => {
    setIsContactEnabled(event.target.checked);
  };

  const onButtonClick = () => {
    setIsButtonPressed(true);

    if (name && isEmailValid && message && isContactEnabled) {
      if (!loading) {
        setSuccess(false);
        setLoading(true);
        setIsButtonDisabled(true);
        timer.current = setTimeout(() => {
          setSuccess(true);
          setLoading(false);
          setIsButtonDisabled(false);
          handleClick();
        }, 500);
      }
    }
  };

  const handleClick = () => setOpen(true);

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const emailC = () => {
    if (isButtonPressed && email === " ") {
      return "Das Feld darf nicht leer sein.";
    } else if (isButtonPressed && !isEmailValid) {
      return "Die Email ist nicht gültig.";
    } else {
      return "";
    }
  };

  return (
    <div className={classes.root}>
      <TextField
        fullWidth
        margin="normal"
        //id={require ? "outlined-required" : "outlined-basic"}
        required
        variant="outlined"
        label="Name"
        name="name"
        /* value={values.name}
        onChange={handleChange}
        error={errors.name}
        helperText={helperText.name} */
        // autoComplete="name"
        onChange={onChangeName}
        error={(isButtonPressed && !name) ?? true}
        helperText={
          isButtonPressed && !name ? "Das Feld darf nicht leer sein" : ""
        }
      />
      <TextField
        fullWidth
        margin="normal"
        //id={require ? "outlined-required" : "outlined-basic"}
        variant="outlined"
        label="Firma"
      />
      <TextField
        fullWidth
        margin="normal"
        //id={require ? "outlined-required" : "outlined-basic"}
        required
        variant="outlined"
        label="Email"
        name="email"
        /* value={values.email}
        onChange={handleChange}
        error={errors.email}
        helperText={helperText.email} */
        onChange={onChangeEmail}
        error={(isButtonPressed && !isEmailValid) ?? true}
        helperText={
          isButtonPressed && !isEmailValid
            ? "Bitte eine gültige Email eingeben."
            : ""
        }
      />
      <TextField
        fullWidth
        margin="normal"
        //id={require ? "outlined-required" : "outlined-basic"}
        variant="outlined"
        label="Telefon"
      />
      {/* <TextareaAutosize
        className={classes.textArea}
        aria-label="empty textarea"
        placeholder="Nachricht"
        required
        rows={5}
      /> */}
      <TextField
        fullWidth
        margin="normal"
        //id={require ? "outlined-required" : "outlined-basic"}
        required
        variant="outlined"
        label="Nachricht"
        name="message"
        multiline
        rows="5"
        /* value={values.message}
        onChange={handleChange}
        error={errors.message}
        helperText={helperText.message} */
        onChange={onChangeMessage}
        error={(isButtonPressed && !message) ?? true}
        helperText={
          isButtonPressed && !message ? "Das Feld darf nicht leer sein" : ""
        }
      />
      <FormControlLabel
        className={classes.label}
        label={
          <span>
            Es gilt die gültige{" "}
            <Link component={GatsbyLink} to="/datenschutz">
              Datenschutzerklärung{" "}
            </Link>
            der newcubator GmbH. Ich willige ein, dass ich über die gewählten
            Kontaktmöglichkeiten, kontaktiert werden darf.
          </span>
        }
        control={<Checkbox color="primary" onChange={onChangeContact} />}
      />
      <Button
        className={classes.button}
        size="large"
        color="primary"
        variant="contained"
        onClick={onButtonClick}
        disabled={isButtonDisabled}
      >
        <Box fontWeight="fontWeightBold">Abschicken</Box>
        {loading && (
          <CircularProgress
            size={28}
            thickness={8}
            variant="indeterminate"
            className={classes.buttonProgress}
          />
        )}
      </Button>
      <>
        {isButtonPressed && !isContactEnabled ? (
          <Box mt={3}>
            <Typography variant="body1" className={classes.redText}>
              Bitte die Datenschutzerklärung bestätigen.
            </Typography>
          </Box>
        ) : (
          <></>
        )}
      </>
      <>
        <Box mt={3}>
          <Snackbar open={open} onClose={handleClose} autoHideDuration={2000}>
            <Alert severity="success">
              <AlertTitle>Vielen Dank für Ihre Nachricht.</AlertTitle>
            </Alert>
          </Snackbar>
        </Box>
      </>
    </div>
  );
};

export default Form;
