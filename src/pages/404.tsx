import React from "react";
import Layout from "../components/layout";

const NotFoundPage = () => (
  <Layout>
    <header>404</header>
  </Layout>
);

export default NotFoundPage;
