import React from "react";
<<<<<<< HEAD
import {
  IoLogoXing,
  IoLogoTwitter,
  IoLogoFacebook,
  IoLogoInstagram,
} from "react-icons/io";
import { Link, Typography } from "@material-ui/core";
import { makeStyles, Theme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
    "@media(max-width: 1060px)": {
      justifyContent: "center",
    },
  },
  textContainer: {
    display: "flex",
  },
  spacing: {
    margin: theme.spacing(4),
  },
  /* icon: {
    margin: theme.spacing(4),
    "&:hover": {
      transform: "scale(1.2)",
      transition: "transform 0.2s",
      color: "rgb(85, 207, 215)",
    },
    "&:not(hover)": {
      transform: "scale(1)",
      transition: "transform 0.2s",
      color: "rgb(255, 255, 255)",
    },
  },
  typography: {
    margin: theme.spacing(4),
  }, */
}));

interface Props {
  xingUrl?: string;
  twitterUrl?: string;
  facebookUrl?: string;
  instagramUrl?: string;
}

const Footer = ({
  xingUrl = "https://www.xing.com/",
  twitterUrl = "https://twitter.com/",
  facebookUrl = "https://de-de.facebook.com/",
  instagramUrl = "https://www.instagram.com/?hl=de",
}: Props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div>
        <Link href={instagramUrl}>
          <IoLogoInstagram size={42} color="#fff" className={classes.spacing} />
        </Link>
        <Link href={twitterUrl}>
          <IoLogoTwitter size={42} color="#fff" className={classes.spacing} />
        </Link>
        <Link href={facebookUrl}>
          <IoLogoFacebook size={42} color="#fff" className={classes.spacing} />
        </Link>
        <Link href={xingUrl}>
          <IoLogoXing size={42} color="#fff" className={classes.spacing} />
        </Link>
      </div>
      <div className={classes.textContainer}>
        <Typography variant="body1" className={classes.spacing}>
          © newcubator 2020
        </Typography>
        <Typography variant="body1" className={classes.spacing}>
          Jobs
        </Typography>
        <Typography variant="body1" className={classes.spacing}>
          Impressum
        </Typography>
        <Typography variant="body1" className={classes.spacing}>
          Datenschutz
        </Typography>
      </div>
=======
import { makeStyles, Theme } from "@material-ui/core/styles";
import { navigate } from "gatsby";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    height: "100px",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    background: theme.palette.primary.main,
  },
}));

interface Props {
  copyrightTo?: string;
}

const Footer = ({ copyrightTo = "Copyright" }: Props) => {
  const classes = useStyles();

  const currentYear = new Date().getFullYear();

  return (
    <div className={classes.root}>
      <button onClick={() => navigate("/page-2")}>Page 2</button>
      <h3>
        {`© ${copyrightTo}`} {currentYear}
      </h3>
>>>>>>> 3aa4ca094ae4776e0bc1d050d1f1b9c322dc2619
    </div>
  );
};

export default Footer;
