import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Img from "gatsby-image/withIEPolyfill";
import { FluidObject } from "gatsby-image";

const useStyles = makeStyles(() => ({
  img: {
    height: "100vh",
  },
}));

interface Props {
  fluid: FluidObject;
  alt?: string;
}

const TopImage = ({ fluid, alt = "Image" }: Props) => {
  const classes = useStyles();

  return (
    <Img fluid={fluid} objectFit="cover" alt={alt} className={classes.img} />
  );
};

export default TopImage;
