import React from "react";
import Hero from "./hero";
import HeroBox from "./hero-box";
import { FluidObject } from "gatsby-image";

interface Props {
  image1: FluidObject;
  image2: FluidObject;
  image3: FluidObject;
  title1?: string;
  title2?: string;
  title3?: string;
}

const HeroSection = ({
  image1,
  image2,
  image3,
  title1 = "Title 1",
  title2 = "Title 2",
  title3 = "Title 3",
}: Props) => (
  <Hero>
    <HeroBox fluid={image1} title={title1} />
    <HeroBox fluid={image2} title={title2} />
    <HeroBox fluid={image3} title={title3} />
  </Hero>
);

export default HeroSection;
