import React from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    height: "500px",
    display: "flex",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    background: theme.palette.secondary.main,
  },
}));

interface Props {
  title?: string;
}

const Banner = ({ title = "Title" }: Props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="h4" component="span">
        {title}
      </Typography>
    </div>
  );
};

export default Banner;
