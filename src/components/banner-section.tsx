import React from "react";
import Hero from "./hero";
import Banner from "./banner";

const BannerSection = () => (
  <Hero>
    <Banner />
    <Banner />
  </Hero>
);

export default BannerSection;
