import React, { ReactNode, useState, useEffect } from "react";
import AppBar, { AppBarProps } from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { CssBaseline, ThemeProvider } from "@material-ui/core";
import { lightTheme, darkTheme } from "../styles/themes";
import ThemeSwitch from "./theme-switch";
import styled from "styled-components";

const StyledToolbar = styled(Toolbar)`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

const StyledLogo = styled.p``;

const StyledThemeSwitch = styled(ThemeSwitch)`
  align-content: flex-end;
`;

const StyledDiv = styled.div`
  display: flex;
  flex-direction: column;
`;

const getPrefColorScheme = () => {
  if (!window.matchMedia) return;
  return window.matchMedia("(prefers-color-scheme: dark)").matches;
};

const getInitialMode = () => {
  const isReturningUser = "dark" in localStorage;
  const savedMode = JSON.parse(localStorage.getItem("dark") || "{}");
  const userPrefersDark = getPrefColorScheme();
  if (isReturningUser) {
    return savedMode;
  } else if (userPrefersDark) {
    return true;
  } else {
    return false;
  }
};
interface Props {
  appBarPosition?: AppBarProps["position"];
  appBarBackground?: string;
  children: ReactNode;
}

const Layout = ({
  appBarPosition = "fixed",
  appBarBackground = "rgba(125,125,125,0.25)",
  children,
}: Props) => {
  const [darkMode, setDarkMode] = useState(getInitialMode);

  const toggleTheme = () => {
    setDarkMode((prevMode: boolean) => !prevMode);
  };

  useEffect(() => {
    localStorage.setItem("dark", JSON.stringify(darkMode));
  }, [darkMode]);

  return (
    <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
      <CssBaseline />
      <AppBar
        position={appBarPosition}
        elevation={12}
        style={{ background: appBarBackground }}
      >
        <StyledToolbar>
          <StyledLogo>Logo</StyledLogo>
          <StyledThemeSwitch checked={darkMode} toggleTheme={toggleTheme} />
        </StyledToolbar>
      </AppBar>
      <StyledDiv>{children}</StyledDiv>
    </ThemeProvider>
  );
};

export default Layout;

/* import React, { ReactNode, useState, useEffect } from "react";
import AppBar, { AppBarProps } from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { makeStyles } from "@material-ui/core/styles";
import { CssBaseline, ThemeProvider } from "@material-ui/core";
import { lightTheme, darkTheme } from "../styles/themes";
import ThemeSwitch from "./theme-switch";

const useStyles = makeStyles(() => ({
  toolbar: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
  },
  themeSwitch: {
    alignContent: "flex-end",
  },
  container: {
    display: "flex",
    flexDirection: "column",
  },
}));

const getPrefColorScheme = () => {
  if (!window.matchMedia) return;
  return window.matchMedia("(prefers-color-scheme: dark)").matches;
};

const getInitialMode = () => {
  const isReturningUser = "dark" in localStorage;
  const savedMode = JSON.parse(localStorage.getItem("dark") || "{}");
  const userPrefersDark = getPrefColorScheme();
  if (isReturningUser) {
    return savedMode;
  } else if (userPrefersDark) {
    return true;
  } else {
    return false;
  }
};
interface Props {
  appBarPosition?: AppBarProps["position"];
  appBarBackground?: string;
  children: ReactNode;
}

const Layout = ({
  appBarPosition = "fixed",
  appBarBackground = "rgba(125,125,125,0.25)",
  children,
}: Props) => {
  const classes = useStyles();
  const [darkMode, setDarkMode] = useState(getInitialMode);

  const toggleTheme = () => {
    setDarkMode((prevMode: boolean) => !prevMode);
  };

  useEffect(() => {
    localStorage.setItem("dark", JSON.stringify(darkMode));
  }, [darkMode]);

  return (
    <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
      <CssBaseline />
      <AppBar
        position={appBarPosition}
        elevation={12}
        style={{ background: appBarBackground }}
      >
        <Toolbar className={classes.toolbar}>
          <p>Logo</p>
          <div className={classes.themeSwitch}>
            <ThemeSwitch checked={darkMode} toggleTheme={toggleTheme} />
          </div>
        </Toolbar>
      </AppBar>
      <div className={classes.toolbar}>{children}</div>
    </ThemeProvider>
  );
};

export default Layout; */
