import React from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import Img from "gatsby-image/withIEPolyfill";
import { FluidObject } from "gatsby-image";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    position: "relative",
    display: "flex",
    transition: "flex 250ms linear",
    "&:hover": {
      flex: 3,
      transform: "scale(1.1)",
      transition: "500ms",
      [theme.breakpoints.up("md")]: {
        flex: 1,
      },
    },
    "&:not(hover)": {
      flex: 1,
      transform: "scale(1)",
      transition: "500ms",
    },
    [theme.breakpoints.only("xs")]: {
      height: "calc(500px / 3)",
    },
  },
  image: {
    width: "100%",
  },
  typography: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
}));

interface Props {
  title: string;
  fluid: FluidObject;
}

const HeroBox = ({ title, fluid }: Props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Img
        fluid={fluid}
        objectFit="cover"
        alt="Job Page Image"
        className={classes.image}
      />
      <Typography variant="h4" className={classes.typography}>
        {title}
      </Typography>
    </div>
  );
};

export default HeroBox;
