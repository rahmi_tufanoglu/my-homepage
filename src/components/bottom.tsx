import React from "react";
import { navigate } from "gatsby";
import { withTheme } from "@material-ui/core/styles";
import styled from "styled-components";

const StyledDiv = withTheme(styled.div`
  height: 100px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background: ${props => props.theme.palette.primary.main};
`);

interface Props {
  copyrightTo?: string;
}

const Bottom = ({ copyrightTo = "Copyright" }: Props) => {
  const currentYear = new Date().getFullYear();

  return (
    <StyledDiv>
      <button onClick={() => navigate("/page-2")}>Page 2</button>
      <h3>
        {`© ${copyrightTo}`} {currentYear}
      </h3>
    </StyledDiv>
  );
};

export default Bottom;
