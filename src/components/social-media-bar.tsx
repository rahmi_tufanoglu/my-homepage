import React from "react";
import {
<<<<<<< HEAD
  IoLogoXing,
  IoLogoTwitter,
  IoLogoFacebook,
  IoLogoInstagram,
  IoLogoYoutube,
} from "react-icons/io";
import { Link } from "@material-ui/core";
import styled, { css } from "styled-components";

const StyledDiv = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-evenly;
  width: 100%;
  height: 100px;
  background: rgba(0, 0, 0);
`;

const iconStyle = css`
  &:hover {
    transform: scale(1.2);
    transition: transform 0.2s;
    color: rgb(85, 207, 215);
  }
  &:not(hover) {
    transform: scale(1);
    transition: transform 0.2s;
    color: rgb(255, 255, 255);
  }
`;

const StyledIoLogoXing = styled(IoLogoXing)`
  ${iconStyle}
`;

const StyledIoLogoTwitter = styled(IoLogoTwitter)`
  ${iconStyle}
`;

const StyledIoLogoFacebook = styled(IoLogoFacebook)`
  ${iconStyle}
`;

const StyledIoLogoInstagram = styled(IoLogoInstagram)`
  ${iconStyle}
`;

const StyledIoLogoYoutube = styled(IoLogoYoutube)`
  ${iconStyle}
`;
=======
  FaXing,
  FaTwitter,
  FaFacebook,
  FaInstagram,
  FaYoutube,
} from "react-icons/fa";
import { Link, makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "space-evenly",
    width: "100%",
    height: "100px",
    background: "rgba(0, 0, 0)",
  },
  icon: {
    "&:hover": {
      transform: "scale(1.2)",
      transition: "transform 0.2s",
      color: "rgb(85, 207, 215)",
    },
    "&:not(hover)": {
      transform: "scale(1)",
      transition: "transform 0.2s",
      color: "rgb(255, 255, 255)",
    },
  },
}));
>>>>>>> 3aa4ca094ae4776e0bc1d050d1f1b9c322dc2619

interface SocialMediaBarProps {
  iconSize?: number;
  iconColor?: string;
  xingUrl?: string;
  twitterUrl?: string;
  facebookUrl?: string;
  instagramUrl?: string;
  youtubeUrl?: string;
}

const SocialMediaBar = ({
  xingUrl = "https://www.xing.com/",
  twitterUrl = "https://twitter.com/",
  facebookUrl = "https://de-de.facebook.com/",
  instagramUrl = "https://www.instagram.com/?hl=de",
  youtubeUrl = "https://www.youtube.com/?gl=DE",
  iconSize = 42,
<<<<<<< HEAD
}: SocialMediaBarProps) => (
  <StyledDiv>
    <Link href={xingUrl}>
      <StyledIoLogoXing size={iconSize} />
    </Link>
    <Link href={twitterUrl}>
      <StyledIoLogoTwitter size={iconSize} />
    </Link>
    <Link href={facebookUrl}>
      <StyledIoLogoFacebook size={iconSize} />
    </Link>
    <Link href={instagramUrl}>
      <StyledIoLogoInstagram size={iconSize} />
    </Link>
    <Link href={youtubeUrl}>
      <StyledIoLogoYoutube size={iconSize} />
    </Link>
  </StyledDiv>
);
=======
}: SocialMediaBarProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Link href={xingUrl}>
        <FaXing size={iconSize} className={classes.icon} />
      </Link>
      <Link href={twitterUrl}>
        <FaTwitter size={iconSize} className={classes.icon} />
      </Link>
      <Link href={facebookUrl}>
        <FaFacebook size={iconSize} className={classes.icon} />
      </Link>
      <Link href={instagramUrl}>
        <FaInstagram size={iconSize} className={classes.icon} />
      </Link>
      <Link href={youtubeUrl}>
        <FaYoutube size={iconSize} className={classes.icon} />
      </Link>
    </div>
  );
};
>>>>>>> 3aa4ca094ae4776e0bc1d050d1f1b9c322dc2619

export default SocialMediaBar;
