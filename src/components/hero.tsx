import React, { ReactNode } from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: "flex",
    height: "500px",
    width: "100%",
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
    },
  },
}));

interface Props {
  children?: ReactNode;
}

const Hero = ({ children }: Props) => {
  const classes = useStyles();

  return <div className={classes.root}>{children}</div>;
};

export default Hero;
