import React from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { Typography, Avatar, Button, Box } from "@material-ui/core";
import { FluidObject } from "gatsby-image";
import Img from "gatsby-image/withIEPolyfill";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlign: "center",
  },
  avatar: {
    width: "80px",
    height: "80px",
  },
  button: {
    width: "80%",
    height: "60px",
  },
}));

interface Props {
  avatar?: FluidObject;
  isButtonEnabled?: boolean;
  isLinkEnabled?: boolean;
  text?: string;
  buttonText?: string;
}

const ServiceBox = ({
  avatar,
  isButtonEnabled,
  isLinkEnabled,
  text,
  buttonText,
}: Props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Img fluid={avatar} objectFit="cover" className={classes.avatar}>
        <Avatar alt="Image" src="/images/gatsby-icon.png" />
      </Img>
      <Box mt={4} mb={4}>
        <Typography variant="body1">PROOF OF CONCEPT</Typography>
      </Box>
      <Box mb={3}>
        <Typography variant="body1">
          Die Machbarkeit eines Vorhabens zu analysieren, bringt Sicherheit in
          den folgenden Entwicklungsprozess. In dieser Phase erarbeiten wir mit
          euch das passende Architekturkonzept für eure Anforderungen und
          Rahmenbedingungen
        </Typography>
      </Box>
      <Button variant="contained" color="primary" className={classes.button}>
        MEHR ERFAHREN
      </Button>
    </div>
  );
};

export default ServiceBox;
