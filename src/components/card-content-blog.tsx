import React from "react";
import { Button, Typography } from "@material-ui/core";
import { navigate } from "gatsby";

interface Props {
  title?: string;
  author?: string;
  date?: string;
  excerpt?: string;
  slug?: string;
}

const CardContentBlog = ({
  title,
  author,
  date,
  excerpt,
  slug = "",
}: Props) => {
  return (
    <>
      <Button color="secondary" onClick={() => navigate(slug)}>
        Click
      </Button>
      <Typography variant="body1" component="span">
        {title}
      </Typography>
      <Typography variant="body1" component="span">
        {author}
      </Typography>
      <Typography variant="body1" component="span">
        {date}
      </Typography>
      <Typography variant="body1" component="span">
        {excerpt}
      </Typography>
    </>
  );
};

export default CardContentBlog;
