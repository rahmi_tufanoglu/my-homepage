import React from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { Avatar, Typography } from "@material-ui/core";
import { FluidObject } from "gatsby-image";
import Img from "gatsby-image/withIEPolyfill";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    background: "red",
    width: "100%",
    display: "flex",
    [theme.breakpoints.down("sm")]: {
      flexWrap: "wrap",
      justifyContent: "center",
    },
  },
  avatar: {
    width: "200px",
  },
  continuousText: {
    background: "green",
    marginTop: theme.spacing(10),
    marginLeft: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      marginTop: theme.spacing(2),
      marginLeft: 0,
    },
  },
}));

interface Props {
  profilePicture?: FluidObject;
  profilePictureAlt?: string;
  name?: string;
  position?: string;
  continuousText?: string;
}

const test =
  "ejflwknrgnregörwmgmlfmjkshfew bfwenfoewjfn al fnaöfnewflw heiepij eowgnrogk pwngiwegoöwendlsmfksdf orgerjn rongerij iorger pwjgrregerlkgmerlkgj rigjnkrgner rkgrgner rkgerkngener regeirjgelgme reogeiorgelet hoetnhlemheg erl";

const MemberBox = ({
  profilePicture,
  profilePictureAlt = "Avatar",
  name = "Name",
  position = "Position",
  continuousText = test,
}: Props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div>
        <Img
          fluid={profilePicture}
          objectFit="cover"
          className={classes.avatar}
        >
          <Avatar alt={profilePictureAlt} src="/images/gatsby-icon.png" />
        </Img>
      </div>
      <div className={classes.continuousText}>
        <Typography variant="body1">{name}</Typography>
        <Typography variant="body1">{position}</Typography>
        <Typography variant="body1">{continuousText}</Typography>
      </div>
    </div>
  );
};

export default MemberBox;
