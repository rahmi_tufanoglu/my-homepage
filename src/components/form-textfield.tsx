import React from "react";
import { Theme, makeStyles } from "@material-ui/core/styles";
import { TextField } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => ({
  textField: {
    width: "100%",
    marginBottom: theme.spacing(3),
  },
}));

interface Props {
  label?: string;
  required?: boolean;
  value?: any;
  onChange?: any;
}

const FormTextfield = ({
  label = "Label",
  required,
  value,
  onChange,
}: Props) => {
  const classes = useStyles();

  return (
    <TextField
      className={classes.textField}
      value={value}
      required={required}
      id={required ? "outlined-required" : "outlined-basic"}
      variant="outlined"
      label={label}
      InputProps={
        {
          //className: classes.textFieldInput,
        }
      }
      onChange={onChange}
    />
  );
};

export default FormTextfield;
