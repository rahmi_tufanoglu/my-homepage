import React from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    height: "500px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: theme.palette.primary.main,
  },
}));

interface Props {
  title?: string;
}

const About = ({ title = "Title" }: Props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="h2">{title}</Typography>
    </div>
  );
};

export default About;
