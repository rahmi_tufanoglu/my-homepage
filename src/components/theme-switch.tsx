import React from "react";
import WbIncandescentIcon from "@material-ui/icons/WbIncandescent";
import Brightness3Icon from "@material-ui/icons/Brightness3";
import { makeStyles, Theme } from "@material-ui/core/styles";
import { Switch } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: "flex",
    height: "100%",
    alignItems: "center",
  },
  switch: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
}));

interface Props {
  checked: boolean;
  toggleTheme: () => void;
}

const ThemeSwitch = ({ checked, toggleTheme }: Props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <WbIncandescentIcon color="primary" />
      <Switch
        checked={checked}
        onChange={toggleTheme}
        className={classes.switch}
      />
      <Brightness3Icon color="secondary" />
    </div>
  );
};

export default ThemeSwitch;
