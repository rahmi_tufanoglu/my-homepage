import { useState, ChangeEvent } from "react";

const useForm = () => {
  const [isContactEnabled, setIsContactEnabled] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(false);
  const [isButtonPressed, setIsButtonPressed] = useState(false);

  const [values, setValues] = useState({
    name: "",
    email: "",
    message: "",
  });

  const [errors, setErrors] = useState({
    name: false,
    email: false,
    message: false,
  });

  const [helperTexts, setHelperTexts] = useState({
    name: "",
    email: "",
    message: "",
  });

  const handleOnChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;

    setValues({
      ...values,
      [name]: value,
    });
  };

  const handleError = (event: ChangeEvent<HTMLInputElement>) => {
    if (isButtonPressed && !event.target.value) {
      return true;
    } else {
      return false;
    }
  };

  const handleHelperText = (event: ChangeEvent<HTMLInputElement>) => {
    if (isButtonPressed && !event.target.value) {
      if (event.target.name === "name") {
        return "Bitte einen Namen eingeben.";
      }
      if (event.target.name === "email") {
        return "Bitte eine Email eingeben.";
      }
      if (event.target.name === "message") {
        return "Bitte eine Nachricht eingeben.";
      }
    } else {
      return "";
    }
  };

  const emailCheck = (e: string) => {
    const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    reg.test(e.toLowerCase()) ? setIsEmailValid(true) : setIsEmailValid(false);
  };

  return [
    isContactEnabled,
    isEmailValid,
    isButtonPressed,
    values,
    errors,
    helperTexts,
  ];
};

export default useForm;
