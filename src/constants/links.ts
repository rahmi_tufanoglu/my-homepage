export default [
  {
    path: "/",
    text: "home",
  },
  {
    path: "/software",
    text: "software",
  },
  {
    path: "/music",
    text: "music",
  },
  {
    path: "/contact",
    text: "contact",
  },
];
