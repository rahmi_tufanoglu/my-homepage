import React from "react";
import {
  FaXing,
  FaTwitter,
  FaFacebook,
  FaInstagram,
  FaYoutube,
} from "react-icons/fa";

export default [
  {
    icon: <FaXing size={42} />,
    url: "https://www.xing.com/",
  },
  {
    icon: <FaTwitter size={42} />,
    url: "https://https://twitter.com/login?lang=de",
  },
  {
    icon: <FaFacebook size={42} />,
    url: "https://https://de-de.facebook.com",
  },
  {
    icon: <FaInstagram size={42} />,
    url: "https://https://www.instagram.com/?hl=de",
  },
  {
    icon: <FaYoutube size={42} />,
    url: "https://www.youtube.com/?gl=DE",
  },
];
