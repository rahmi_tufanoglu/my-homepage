export default function validate(values: any) {
  const errors = {
    email: "",
  };
  if (!values.email) {
    errors.email = "Bitte die Email eingeben.";
  } else if (!/\S+@\S+\.\S+/.test(values.email)) {
    errors.email = "Email ist ungültig.";
  }
  return errors;
}
