import "styled-components";
import { Breakpoints } from "@material-ui/core/styles/createBreakpoints";
import { Spacing } from "@material-ui/core/styles/createSpacing";

declare module "styled-components" {
  export interface DefaultTheme {
    /* keys: ["xs", "sm", "md", "lg", "xl"];
    values: { xs: 0; sm: 600; md: 960; lg: 1280; xl: 1920 };
    up: key => `@media (min-width:${values[key]}px)`; */
    palette: {
      primary: {
        main: string;
      };
      secondary: {
        main: string;
      };
      contrastThreshold: number;
      tonalOffset: number;
      text: {
        primary: string;
        secondary: string;
      };
      background: {
        paper: string;
        default: string;
      };
    };
    spacing: Spacing;
  }
}
