import { createMuiTheme } from "@material-ui/core/styles";

const lightTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#f44336",
    },
    secondary: {
      main: "#ffa726",
    },
    contrastThreshold: 5,
    tonalOffset: 0.2,
    text: {
      primary: "#000",
      secondary: "#fff",
    },
    background: {
      paper: "#fff",
      default: "#fff",
    },
  },
});

const darkTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#7b1fa2",
    },
    secondary: {
      main: "#1e88e5",
    },
    contrastThreshold: 5,
    tonalOffset: 0.2,
    text: {
      primary: "#fff",
      secondary: "#000",
    },
    background: {
      paper: "#000",
      default: "#373737",
    },
  },
});

export { lightTheme, darkTheme };
