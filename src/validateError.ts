export default function validate(values: any) {
  let errors = {};

  if (!values.name) {
    errors = "Bitte einen Namen eingeben.";
  }

  return errors;
}
